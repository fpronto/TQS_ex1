package main;

import java.util.ArrayList;

public class BubbleSort {

    public void sort(ArrayList<Integer> numbers) {
        ArrayList<Integer> numbersSorted = new ArrayList<>();
        int n = numbers.size();
        int temp = 0;
        numbersSorted.add(numbers.get(0));

        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (numbers.get(j - 1) > numbers.get(j)) {
                    temp = numbers.get(j - 1);
                    numbers.set(j - 1, numbers.get(j));
                    numbers.set(j, temp);
                }
            }
        }
        for (int i = 0; i<numbers.size(); i++) {
            System.out.println(numbers.get(i));
        }
    }
}
