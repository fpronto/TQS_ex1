package main;

import java.util.ArrayList;

public class Main {
    public static void main(String args[]) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(30);
        numbers.add(27);
        numbers.add(12);
        numbers.add(85);
        numbers.add(58);
        numbers.add(47);
        numbers.add(96);
        numbers.add(30);

        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(numbers);
    }
}
